function reduce(elements, cb, startingValue){
    let i;
    if (startingValue) {
        i= startingValue;
    }
    else {
        i=0;
    }
    const val = elements[i];
    let reduced = val;
    for (let j=i+1; j< elements.length; j++) {
        reduced = cb(reduced, elements[j]);
    }
    return reduced;
}
module.exports = reduce;