function find(elements, cb) 
{   
    for (var i=0; i < elements.length; i++) 
    {
        if (cb(elements[i])) 
        {
            found= 1;
            break;
        }
    }
    if(found==1){
        return elements[i];
    }
    else {
        return;
    }
}
module.exports = find;