function each(elements, cb) {
    if (elements.length==0){
        cb(elements);
    }
    for (let i = 0; i < elements.length; i++) {
            cb(elements[i]);
    }
}
module.exports = each;